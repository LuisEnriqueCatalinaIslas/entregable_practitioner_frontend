import { LitElement, html, css } from 'lit-element';

class ProductsList extends LitElement {

    static get styles() {
        return css `
      :host {
        display: block;
      }
    `;
    }

    static get properties() {
        return {
            productos: { type: Array }
        };
    }

    constructor() {
        super();
        this.productos = [];
        this.productos.push({ "nombre": "Movil XL", "descripcion": "Un teléfono grande con una de las mejores pantallas" });
        this.productos.push({ "nombre": "Movil Mini", "descripcion": "Un teléfono mediano con una de las mejores caramaras" });
        this.productos.push({ "nombre": "Movil Standard", "descripcion": "Un teléfono estandar.Nada especial" });
    }

    render() {
            return html `
      <div class = "contenedor">
        ${this.productos.map (p => html `<div class = "producto"><h3>${p.nombre}</h3><h3>${p.descripcion}</h3></div> `)}
      </div>
    `;
    }
    createRenderRoot(){
        return this;
    }
}

customElements.define('products-list', ProductsList);