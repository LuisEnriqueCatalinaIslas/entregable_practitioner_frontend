import { LitElement, html, css } from 'lit-element';

class TestXmlhttp extends LitElement {

    static get styles() {
        return css `
      :host {
        display: block;
      }
    `;
    }

    static get properties() {
        return {
            planet: { type: Object }
        };
    }

    constructor() {
        super();
        this.cagarPlaneta();
    }

    render() {
        return html `
      <p><code>${this.planet}</code></p>
    `;
    }
    cagarPlaneta() {
        //xmlHttpRequest
        var req = new XMLHttpRequest();
        req.open("GET", "https://swapi.dev/api/planets/1/", true);
        req.onreadystatechange =
            ((aEvt) => {
                if (req.readyState === 4) {
                    if (req.status === 200) {
                        this.planet = req.responseText;
                        this.requestUpdate();
                    } else {
                        alert("Error enviando rest");
                    }
                }
            });
        req.send(null);
    }
}

customElements.define('test-xmlhttp', TestXmlhttp);