Feature: Lista Productos
    Scenario: Cargar Lista de Productos
        When we request the products list
        Then we should receive
            | nombre | descripcion |
            | Movil XL | Un teléfono grande con una de las mejores pantallas |
            | Movil Mini | Un teléfono mediano con una de las mejores caramaras |
            | Movil Standard | Un teléfono estandar.Nada especial |