import { LitElement, html, css } from 'lit-element';
import { OrigenPersona } from '../origen-persona/origen-persona.js';
class FichaPersona extends LitElement {

    static get styles() {
        return css `
      div {
        border : 1px solid;
        border-radius : 10px;
        padding : 10px;
        margin : 10px;
      }
    `;
    }

    static get properties() {
        return {
            nombre: { type: String },
            apellidos: { type: String },
            anyosAntiguieda: { type: String },
            foto: { type: Object },
            nivel: { type: String },
            bg: { type: String },
            origen: { type: String }
        };
    }

    constructor() {
        super();
        this.nombre = "Pedro";
        this.apellidos = "Lopez Lopez";
        this.anyosAntiguieda = 4;
        this.foto = {
            scr: "./src/ficha-persona/img/persona.jpg",
            alt: "Foto Persona"
        };
        this.bg = "AliceBlue";
    }

    render() {
        return html `
        <div style="width : 400px; background-color:${this.bg};">
            <label for="lnombre">Nombre</label>
            <input type="text" id="inombre" name="inombre" value="${this.nombre}"
            @input=${this.updateNombre}/>
            <br/>
            <label for="lapellidos">Apellidos</label>
            <input type="text" id="iapellidos" name="iapellidos" value="${this.apellidos}"/>
            <br/>
            <label for="lantiguedad">Antiguedad</label>
            <input type="number" id="iantiguedad" name="iantiguedad" value="${this.anyosAntiguieda}"
            @input=${this.updateAntiguedad}/>
            <br/>
            <label for="lnivel ">Nivel</label>
            <input type="text" id="inivel" name="inivel" value="${this.nivel}" disabled/>
            <br/>

            <origen-persona @origen-set=${this.origenChange}></origen-persona>
            <br/>
            <img src="${this.foto.scr}" height="200" width="200"  alt="${this.foto.alt}"/>
        </div>
      
    `;
    }
    updated(changedProperties) {
        if (changedProperties.has("nombre")) {
            console.log("Propiedad Nombre Cambiada. Valor anterior " +
                changedProperties.get("nombre") + " Valor Nuevo " + this.nombre);
        }
        if (changedProperties.has("anyosAntiguieda")) {
            this.actualizarNivel();
        }
    }
    updateNombre(e) {
        this.nombre = e.target.value;
    }
    updateAntiguedad(e) {
        this.anyosAntiguieda = e.target.value;
    }
    actualizarNivel() {
        if (this.anyosAntiguieda >= 7) {
            this.nivel = "Chief";
        } else if (this.anyosAntiguieda >= 5) {
            this.nivel = "Senior";
        } else if (this.anyosAntiguieda >= 3) {
            this.nivel = "Team";
        } else {
            this.nivel = "Junior - Esclavo";
        }
    }
    origenChange(e) {
        this.origen = e.detail.message;
        console.log("Valor optenido : " + this.origen);
        if (this.origen === "USA") {
            this.bg = "LightPink";
        } else if (this.origen === "México") {
            this.bg = "LightGreen";
        } else if (this.origen === "Cánada") {
            this.bg = "LightGoldenRodYellow";
        } else if (this.origen === "España") {
            this.bg = "IndianRed ";
        } else {
            this.bg = "GoldenRod";
        }
    }
}

customElements.define('ficha-persona', FichaPersona);